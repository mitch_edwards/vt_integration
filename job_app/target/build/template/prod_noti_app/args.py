"""Job Args"""
from argparse import ArgumentParser


class Args:
    """Job Args"""

    def __init__(self, parser: ArgumentParser):
        """Initialize class properties."""
        parser.add_argument('--owner', required=True)
        parser.add_argument('--vt_api_key', required=True)
        parser.add_argument('--last_cursor', action='store_true')
