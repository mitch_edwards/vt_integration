"""ThreatConnect Job App"""
# VirusTotal Integration 
# Developed by: Mitchell Edwards
# Description - A runtime job that grabs VirusTotal Hunting notifications to pull, parse and upload Files and Signatures to the 
# ThreatConnect platform. 

# Third-party Dependencies 
# Plyara -  third-party library that parses YARA rulesets into individual rules. 
# Hashlib - a library used to calculate cryptographic hashes of various data, here used to create unique IDs out of object data.
# Sys - a library used to interact with a local system, here used for local debugging and eventually a local version.
# JSON - a library used to interact with JSON objects. 
# Time, datetime - libraries used to calculate runtime, current date and previous dates.
import csv, requests, sys, json, time, datetime, plyara
from hashlib import md5

from job_app import JobApp

TEST = True


VT_NOTIFICATIONS_URL = 'https://www.virustotal.com/api/v3/intelligence/hunting_notification_files'
VT_RULESET_URL = 'https://www.virustotal.com/api/v3/intelligence/hunting_rulesets/'


# Mappings object - This object describes relationships between VT fields and TC fields as vt_field : tc_field 
MAPPINGS = {
"attributes": {"description" : "Description", "priority": "Rule Priority", "confidence" : "Rule Confidence"},
"tags": True,
"boolean_tags": ["sandbox_restricted"],
#"associations": {"threats" : "Threat"},
"default_association": "Threat"
}

# Function get_notifications()
# Purpose : 
#   To fetch notifications in a batch and iterate through batches until either a certain notification is reached, or the notification
#   date is outside of a pre-defined window.
# Parameters :
#   vt_api_key : The user's VirusTotal API key
#   limit : Amount of notifications to fetch in one request
#   cursor : an object used to walk through the VT Notification queue iteratively using the value of the last-parsed Notification ID
#   vt_filter : a filter, such as last_id, to apply to the notification fetch.
# Return : 
#   response - an iterable list of Notification objects

def get_notifications(vt_api_key, limit=None, cursor=None, vt_filter=None):
    params = {}
    if limit:
        params['limit'] = limit
    if vt_filter:
        params['filter'] = vt_filter
    if cursor:
        
        params['cursor'] = cursor
    response = requests.get(
        VT_NOTIFICATIONS_URL,
        headers={'x-apikey': vt_api_key},
        params=params
    ).json()
    new_cursor = response.get('meta', {'cursor': None}).get('cursor')
    yield response

    if new_cursor:
        yield from get_notifications(vt_api_key, limit, new_cursor, vt_filter)
    else:
        yield None

# Function daystosecods()
# Purpose : 
#   A quick and ugly function used to calculate the amount of seconds in a certain amount of days, defaulting to 30. 
#   This is used further down to calculate if a notification was created outside of a certain time window.
# Parameters : 
#   days : the amount of days to convert into seconds
# Return : 
#   (dynamic) : the amount of seconds in a given length of days.

def daystoseconds(days=30):
    return days*24*60*60

# Class Notification
# Purpose : 
#   To store all data associated with a VirusTotal Notification object. Upon instantiation, the notification initializes all 
#   data associated with the object and parses information out of the vt_notification parameter. It then checks if the file and signature 
#   associated with the notification is present in the platform, and if they are associated, and if they are not, they will be pushed to the
#   platform and/or the object associations will be added.
# Parameters : 
#   vt_notification : the dictionary object associated with a single VirusTotal notification.
#   vt_api_key : the user's VirusTotal API key
#   tcex : an instantiated ThreatConnect TCEX object
#   args : an arguments object, either from a local system or a hosted system.
# Instantiated Object : 
#   A Notification object with associated data and a context that explicitly requires all files and signatures associated with a VirusTotal 
#   Notification being present in the platform. 

class Notification:

    # SECTION INITIALIZE
    # Description : initializes all data associated with the Notification object

    # Function init()
    # Purpose : 
    #   Initializes and instantiates various datapoints associated with the Notification object.
    # Parameters : 
    #   vt_notification : a user's VirusTotal API key
    #   tcex : an initialized tcex object
    #   args : either a local list of CLI arguments, or a hosted version of system arguments.
    # Return : 
    #   (Object) : a fully initialized Notification object, with the context that all Signatures and Files are present in platform and
    #   associated.
    def __init__(self, vt_notification, vt_api_key, tcex, args):
        self.args = args
        self.sigInPlatform = False
        self.fileInPlatform = False
            
        self.tcex = tcex
        self.batch = self.tcex.batch(self.args.owner)
        self.notification = {}
        self.notification_file = {}
        self.notification_rule = {}
        
        self.instantiateNotificationObject(vt_notification, vt_api_key)
        self.rule_text = self.raw_rule()
        

        
        
        self.sigInPlatform, self.sig_obj = self.checkIfSigInPlatform()
        
        self.fileInPlatform, self.file_obj = self.checkIfFileInPlatform()
        self.associated = self.checkIfAssociated()
        if not self.fileInPlatform:
            self.tcex.log.info('[-] File is not in platform')
            
            self.file_obj = self.addFileToPlatform()
                
            
        
        if not self.sigInPlatform:
            self.tcex.log.info('[-] Sig is not in platform')
            self.sig_obj = self.addSigToPlatform()
            
        if not self.associated:
            self.tcex.log.info('[-] Association not present')
            self.addAssociations()
            
        batch_data = self.batch.submit_all()
        errors = []
        [errors.extend(d.get('errors', [])) for d in batch_data if d.get('errors', None)]
        if errors:
            self.tcex.log.error('[-] Errors during Batch: {}'.format(errors))
            self.tcex.log.error(f'[-] Batch: {str(self.batch)}')


    # Function instantiateNotificationObject()
    # Purpose : 
    #   Instantiates all of the important variables in the Notification object contained within the vt_notification dictionary
    # Parameters : 
    #   vt_notification : a dictionary describing a VirusTotal notification
    # Returns : 
    #   (None)
    #       (This function is a setter: it sets fields associated with the Notification object)
    def instantiateNotificationObject(self, vt_notification='', vt_api_key=''):
        notification = {}
        notification_rule = {}
        notification_file = {}

        self.vt_api_key = vt_api_key
        self.vt_notification = vt_notification 
        self.vt_ruleset_id = vt_notification['context_attributes']['ruleset_id']
        self.vt_rule_name = vt_notification['context_attributes']['rule_name']
        self.ruleset = self.grabRuleset(self.vt_ruleset_id, self.vt_rule_name, self.vt_api_key)
        self.ruleObj = self.rulesetToRule(self.ruleset, self.vt_rule_name)
        
        
       
        self.ruleHash = md5(str(self.ruleObj).encode('utf-8')).hexdigest()
        self.description, self.priority, self.rule_version, self.sharing_profile, self.date, self.rule_hash, self.confidence = self.parseRuleMetadata(self.ruleObj['metadata'])
        
        self.notification_id = vt_notification['context_attributes']['notification_id']
        self.unique_id = self.vt_ruleset_id+'.'+self.vt_rule_name
        
        

        notification_file['md5'] = vt_notification['attributes']['md5']
        notification_file['sha256'] = vt_notification['attributes']['sha256']
        notification_file['sha1'] = vt_notification['attributes']['sha1']
        
        #Size
        notification_file['size'] = '{} (bytes)'.format(str(vt_notification['attributes']['size']))
        #Exif Tool Metadata
        try:
            notification_file['exiftool_data'] = 'Exif Tool Metadata\nSource: VirusTotal Hunting\n\n**{}**'.format(str(vt_notification['attributes']['exiftool']))
        except:
            notification_file['exiftool_data'] = 'Exif Tool Metadata\nSource: VirusTotal Hunting\n\n**{}**'.format('Exif Tool Metadata Not Found')
        #First Seen data
        notification_file['first_seen'] = 'First Seen\nSource: VirusTotal Hunting\n{}'.format(datetime.datetime.fromtimestamp(vt_notification['attributes']['first_submission_date']))
        #Last Seen data
        notification_file['last_seen'] = 'Last Seen\nSource: VirusTotal Hunting\n{}'.format(datetime.datetime.fromtimestamp(vt_notification['attributes']['last_submission_date']))        
        #File Name data
        filename_str = ''
        for item in vt_notification['attributes']['names']:
            filename_str = filename_str+str(item)+', '
        filename_str = filename_str[:len(filename_str)-2]
        notification_file['filenames'] = filename_str

        notification_file['attributes'] = {}
        #File Type data
        try:
            notification_file['attributes']['filetype'] = 'File Type\nSource: VirusTotal Hunting\n{}'.format(vt_notification['attributes']['exiftool']['FileType'])
        except:
            notification_file['attributes']['filetype'] = 'File Type\nSource: VirusTotal Hunting\n{}'.format('Filetype Information Not Found')
        #ssdeep Hash data
        notification_file['attributes']['ssdeep'] = 'ssdeep Hash\nSource: VirusTotal Hunting\n{}'.format(vt_notification['attributes']['ssdeep'])
        #Times Submitted data
        notification_file['attributes']['times_submitted'] = 'Times Submitted\nSource: VirusTotal Hunting\n{}'.format(str(vt_notification['attributes']['times_submitted']))
        #Unique Sources data
        notification_file['attributes']['unique_sources'] = 'Unique Sources\nSource: VirusTotal Hunting\n{}'.format(str(vt_notification['attributes']['unique_sources']))
        #Match in Subfile data
        notification_file['attributes']['match_subfile'] = 'Match in Subfile\nSource: VirusTotal Hunting\n{}'.format(str(vt_notification['context_attributes']['match_in_subfile']))
        #Last Analysis Stats data
        last_analysis_string = 'Confirmed-timeout: {}\nFailure: {}\nHarmless: {}\nMalicious: {}\nSuspicious: {}\nType-Unsupported: {}\nUndetected: {}\n'.format(str(vt_notification['attributes']['last_analysis_stats']['confirmed-timeout']),str(vt_notification['attributes']['last_analysis_stats']['failure']),str(vt_notification['attributes']['last_analysis_stats']['harmless']),str(vt_notification['attributes']['last_analysis_stats']['malicious']),str(vt_notification['attributes']['last_analysis_stats']['suspicious']),str(vt_notification['attributes']['last_analysis_stats']['timeout']), str(vt_notification['attributes']['last_analysis_stats']['type-unsupported']),str(vt_notification['attributes']['last_analysis_stats']['undetected']))
        notification_file['attributes']['last_analysis_stats'] = 'Last Analysis Stats\nSource: VirusTotal Hunting\n{}'.format(last_analysis_string)
        #Last Analysis Results data (Currently doesn't work)
        whitespace = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
        last_analysis = 'Last Analysis Results\nSource: VirusTotal Hunting\n**Engine**{}**Category**{}**Result**\n'.format(whitespace, whitespace)
        total_engines = 0
        total_detected = 0
        
        for engine_obj, engine_details in vt_notification['attributes']['last_analysis_results'].items():
            total_engines = total_engines+1
            engine_name = engine_details['engine_name']
            category = engine_details['category']
            result = engine_details['result']

            if engine_name is None:
                engine_name = 'None'
            if category is None:
                category = 'None'
            if result is None:
                result = 'None'
            last_analysis = last_analysis+engine_name+whitespace+category+whitespace+result+'\n'
            if engine_details['category'] != 'undetected' and engine_details['category'] != 'type-unsupported':
                total_detected = total_detected+1
        notification_file['last_analysis_results'] = last_analysis
        #Detection Ratio data
        notification_file['attributes']['detection_ratio'] = 'Detection Ratio\n{}'.format(str(total_detected)+'/'+str(total_engines))
        notification_file['tags'] = []
        for item in vt_notification['attributes']['tags']:
            
            notification_file['tags'].append(item)

        notification_rule['rule_name'] = self.vt_rule_name
        notification_rule['ruleset_id'] = self.vt_ruleset_id
        #notification_rule['rule_obj'] = ruleObj 
        notification_rule['ruleHash'] = self.ruleHash    
        notification_rule['description'] = self.description
        notification_rule['priority'] = self.priority
        #notification_rule['threats'] = self.threats 
        #notification_rule['threat_type'] = self.threat_type
        notification_rule['rule_version'] = self.rule_version
        notification_rule['sharing_profile'] = self.sharing_profile
        notification_rule['date'] = self.date 
        notification_rule['rule_hash'] = self.rule_hash
        notification_rule['confidence'] = self.confidence

        self.notification_rule = notification_rule
        self.notification_file = notification_file
        
        self.sig_id = self.batch.generate_xid(self.vt_rule_name)
        self.file_id = self.batch.generate_xid(self.notification_file['sha256'])
        self.incident_id = self.batch.generate_xid(self.notification_id)

    # Function parseRuleMetadata
    # Purpose :
    #   Parses data specifically contained in the vt_notification's metadata object
    # Parameters : 
    #   vt_metadata : a list of VirusTotal metadata fields and objects
    # Returns : 
    #   (None)
    #       (This function is a setter: it sets fields associated with the Notification object)
    def parseRuleMetadata(self, vt_metadata=[]):
        
        description = ''
        priority = ''
        #threats=''
        #threat_type=''
        rule_version=''
        sharing_profile=''
        date=''
        rule_hash=''
        confidence = ''
        for item in vt_metadata:
            try:
                description = item['description']
            except:
                pass 
            try:
                priority = item['priority']
            except:
                pass
            #try:
            #    threats = item['threats']
            #except:
            #    pass
            #try:
            #    threat_type = item['threat_type']
            #except:
            #    pass
            try:
                rule_version = item['rule_version']
            except:
                pass
            try:
                sharing_profile = item['sharing_profile']
            except:
                pass
            try:
                date = item['date']
            except:
                pass
            try:
                rule_hash = item['rule_hash']
            except:
                pass
            try:
                confidence = item['confidence']
            except:
                pass
        
        if description == '':
            description = 'No rule description found'
        if priority == '':
            priority = 'No rule description found'
        #if threats == '':
        #    threats = 'No rule threats found'
        #if threat_type == '':
        #    threat_type='No rule threat type found'
        if rule_version == '':
            rule_version = 'v1'
        if sharing_profile == '':
            sharing_profile = 'TLP:AMBER'  
        if date == '':
            date = 'No date given'
        if rule_hash == '':
            rule_hash = 'No rule hash given'  
        if confidence == '':
            confidence = 'No rule confidence given'    


        return description, priority, rule_version, sharing_profile, date, rule_hash,confidence
    

    # END SECTION INITIALIZE
    
    
    # SECTION CHECKS
    # Description : all functions that check for the presence of various objects in the TC platform, as well as their association.

    # Function checkIfFileInPlatform()
    # Purpose : 
    #   Checks if a File object is present in the ThreatConnect platform. It currently searches for files via sha256 hash and MD5 hash in 
    #   the file object's 'summary' field.
    # Parameters : 
    #   (None)
    # Returns : 
    #   fileInPlatform : a boolean variable indicating whether or not the File object was found on the platform
    def checkIfFileInPlatform(self):
           
            
            parameters = {'includes': ['additional', 'attributes', 'labels', 'tags']}
            filters = self.tcex.ti.filters()
            filters.add_filter('summary','^',self.notification_file['sha256'])
            files = self.tcex.ti.indicator(indicator_type='File', owner=self.args.owner)
            count = 0
            file_id = ''
            fileInPlatform = False
            files_arr = []
            file_obj = {}
            target_file = {}
            for File in files.many(filters=filters, params=parameters):
                count = count+1
                file_id = str(File['id'])
                target_file = self.batch.indicator(indicator_type='File', xid=file_id, summary = self.notification_file['sha256'])
                files_arr.append(target_file)
                
            if count == 0:
                filters = self.tcex.ti.filters()
                filters.add_filter('summary', '^', self.notification_file['md5'])
                for File in files.many(filters=filters, params=parameters):
                    count = count+1
                    file_id = File['id']
                    target_file = self.batch.indicator(indicator_type='File', xid=file_id, summary = self.notification_file['sha256'])
                    return True, target_file
            
            if count == 0:
                fileInPlatform = False
                file_obj = {}
                
            else:
                file_obj = self.batch.indicator(indicator_type='File', xid=file_id, summary = self.notification_file['sha256'])
                fileInPlatform = True
            file_obj.add_association(self.incident_id)
            return fileInPlatform, file_obj

    # Function checkIfSigInPlatform()
    # Purpose : 
    #   Checks if a Signature object is present in the ThreatConnect platform. It currently searches for signatures via the rule name 
    #   present in the VirusTotal notification object, initialized in the instantiateNotificationObject function.
    # Parameters : 
    #   (None)
    # Returns : 
    #   fileInPlatform : a boolean variable indicating whether or not the File object was found on the platform
    def checkIfSigInPlatform(self):
        
        parameters = {'includes': ['additional', 'attributes', 'labels', 'tags']}
        filters = self.tcex.ti.filters()
        filters.add_filter('name','^',self.vt_rule_name)
        groups = self.tcex.ti.group(group_type='Signature', owner=self.args.owner).many(params=parameters, filters=filters)
        sigInPlatform = ''
        count = 0
        sig_obj = {}
        for group in groups:
            count = count+1
            if group['name'] == self.vt_rule_name:
                sigInPlatform = True
                id = group['id']
                sig_obj = self.batch.group(group_type='Signature', name = self.vt_rule_name, file_name = self.vt_rule_name, file_type='YARA', file_text = self.rule_text, xid = str(self.sig_id))
                return True, sig_obj
        if not sigInPlatform:
            id = ''
            sig_obj = {}
        return sigInPlatform, sig_obj

    # Function checkIfAssociated
    # Purpose : 
    #   Checks if a File object (explicitly in platform) is associated with a Signature object (also explicitly in platform)
    # Parameters : 
    #   (None)
    # Returns :
    #   (None)
    #       (This function is a setter: it sets fields associated with the Notification object) 
    def checkIfAssociated(self):
        
        parameters = {'includes': ['additional', 'attributes', 'labels', 'tags', 'associations']}
        indicator = self.file_obj
        try:
            for assoc in indicator.group_associations():
                if str(assoc['id']) == self.sig_id:
                    return True
        except:
            pass

        return False
    
    # END SECTION CHECKS


    # SECTION PUSHES
    # Description : all functions that push objects and data to the TC platform

    # Function createIncident()
    # Purpose : 
    #   Creates a batch.group() object of type Incident and adds all key attributes to the incident. This incident serves as a placeholder containing information 
    #   pertaining to the actual VirusTotal notification, as opposed to the Signature and File associated to the incident.
    # Parameters : 
    #   indicator : a batch.indicator() object representing a "recreated" indicator object to associate with the Incident. 
    #   signature : a batch.signature() object representing a "recreated" signature object to associate with the Incident.
    # Returns : 
    #   incident_object : a batch.group() object of type "Incident" that serves as an intermediary object associated with the indicator and signature objects.

    def createIncident(self):
        parameters = {
            "includes": ["additional", "attributes", "labels", "tags"]
        }
        
        #batch = self.tcex.batch('VirusTotal Hunting')
        sid = str(self.notification_id)



        noti_date = self.vt_notification['context_attributes']['notification_date']
        noti_date_readable = str(datetime.datetime.fromtimestamp(noti_date))
        
        incident_name = f'[{str(sid[len(sid)-5:len(sid)])}] Hit on ruleset ID:rule {str(self.vt_ruleset_id)}:{str(self.vt_rule_name)}'

        description = 'Notification Date: {}\nNotification Date (timestamp): {}\n\n'.format(noti_date_readable, noti_date)
        description = description+'**File Information**\nMD5:{}\nSHA256:{}\n{}\n'.format(self.notification_file['md5'], self.notification_file['sha256'], self.notification_file['attributes']['filetype']) 

        incident_object = self.batch.group(group_type='Incident', summary = description, name=incident_name, xid=self.incident_id)
        
        incident_object.attribute('Description', description, True)
        incident_object.add_key_value('eventDate', 'today')
        
        self.incident_obj = incident_object

    # Function addAssociations()
    # Purpose : 
    #   In the context of a File object and a Signature object explicitly not being associated, this function adds an association
    #   between the two as well as to an associated Incident object created above.
    # Parameters : 
    #   (None)
    # Returns : 
    #   (None)
    #       This function merely pushes an exact copy of the files, but with associations. 
    def raw_rule(self):
        raw_rule = 'rule '+self.vt_rule_name+' {\n\n'
        try:
            for line in self.ruleObj['raw_meta'].split('\n'):
                raw_rule = raw_rule+'\t'+line+'\n'
        except:
            pass
        try:
            for line in self.ruleObj['raw_strings'].split('\n'):
                raw_rule = raw_rule+'\t'+line+'\n'
        except:
            pass
        try:
            for line in self.ruleObj['raw_condition'].split('\n'):
                raw_rule = raw_rule+'\t'+line+'\n'
        except:
            pass
        raw_rule = raw_rule+'}'

        return raw_rule
    
    def addAssociations(self):
        #indicator = self.file_obj
        
        signature = self.sig_obj
        # signature.attribute('Rule Confidence', self.confidence)
        # signature.attribute('Rule Priority', self.priority)
        #self.sig_obj.security_label(self.sharing_profile)
        self.createIncident()
        self.incident_obj.association(self.sig_id)
        self.incident_obj.association(self.file_id)
        #self.incident_obj.association(self.file_id)
        # This is messy but because the tcex library is absolutely wonked out of its mind I have to do it.
        
        try:
            self.file_obj.association(self.sig_id)
        except:
            self.file_obj.add_association(self.sig_obj)
            
        
    
    # Function addFileToPlatform()
    # Purpose : 
    #   Adds a File object to the ThreatConnect platform using data pulled from the notification_file object, which is instantiated in the instantiateNotificationObject() function
    # Parameters : 
    #   (None)
    # Returns : 
    #   new_file : a batch.file() object containing information pertaining to the File object newly added to the platform
    def addFileToPlatform(self):
        
        #batch = self.tcex.batch('VirusTotal Hunting')
        
        md5 = self.notification_file['md5']
        sha256 = self.notification_file['sha256']
        sha1 = self.notification_file['sha1']
        
        description = '\n Added to platform on {} by VirusTotal Hunting Integration'.format(str(datetime.datetime.now(datetime.timezone.utc)))
        #description = Default_Description+'\n\n{}'.format(self.notification_file['last_analysis_results'])
        
        new_file = self.batch.file(md5=md5, sha256 = sha256, sha1=sha1, xid=self.file_id)
        new_file.attribute('Description', description, True)
        for name, value in self.notification_file['attributes'].items():
            
            attribute_name = value.split('\n')[0]
            values = value.split('\n')
            value = values[len(values)-1]
        for tag in self.notification_file['tags']:
            new_file.tag(name = tag)
        
        new_file.security_label(self.sharing_profile)
        #self.batch.save(new_file)
        #batch_data = self.batch.submit_all()
        # errors = []
        # [errors.extend(d.get('errors', [])) for d in batch_data if d.get('errors', None)]
        # if errors:
        #     self.tcex.log.error('Errors during Batch: {}'.format(errors))
        
        return new_file

    # Function addSigToPlatform()
    # Purpose : 
    #   Adds a Signature to the ThreatConnect platform using data pulled from the notification_rule object, which is instantiated in the instantiateNotificationObject() function and
    #   the rulesetToRule() function.
    # Parameters : 
    #   (None)
    # Returns : 
    #   rule_obj : a batch.group() object of type Signature containing data pertaining to the newly added Signature/YARA rule

    def addSigToPlatform(self):
        
        #batch = self.tcex.batch('VirusTotal Hunting')
       

        try:
            meta = self.ruleObj['metadata']
            
            tags = []
            for item in meta:
                try:
                    tags = item['tags'].split(',')
                except:
                    pass
            
        except:
            tags = []
        
        rule_obj = self.batch.group(group_type='Signature', name=self.vt_rule_name, file_name=self.vt_rule_name+'.yara', file_type='YARA', file_text=self.rule_text, xid=str(self.sig_id))
        
        rule_obj.attribute('Rule Confidence', self.confidence)
        rule_obj.attribute('Rule Priority', self.priority)
        rule_obj.security_label(self.sharing_profile)

        for tag in tags:
            rule_obj.tag(tag)

        return rule_obj

    # END SECTION PUSHES

    
    # Function grabRuleset()
    # Purpose : 
    #   Hits the VirusTotal API to grab rulesets containing the rule that triggered the notification
    # Parameters : 
    #   id : the VirusTotal ruleset ID
    #   rule_name : the name of the rule that triggered the notification
    #   vt_api_key : the user's VirusTotal API key
    # Returns : 
    #   rules : a dictionary containing the ruleset fetched from the VirusTotal API
    def grabRuleset(self, id='', rule_name='', vt_api_key=''):
        
        req = requests.get(VT_RULESET_URL+id, headers={'x-apikey':vt_api_key})
        rules = req.json()['data']['attributes']['rules']
        
        return rules
    
    # Function rulesetToRule()
    # Purpose : 
    #   Pulls an individual rule from a ruleset.
    # Parameters : 
    #   ruleset : a string containing all the rules in a ruleset.
    # Returns : 
    #   ruleObj : a dictionary representing the target rule object.
    def rulesetToRule(self, ruleset='', rule_name=''):
        ruleObj = {}
        try:
            parser = plyara.Plyara()
            plyara_obj = parser.parse_string(ruleset)
            for item in plyara_obj:
                if item['rule_name'] == rule_name:
                    ruleObj = item
        except Exception as e:
            pass
        return ruleObj
    
# Class App
# Purpose : 
#   This is the main Application that instantiates the tcex object either locally or in platform. The run() function contains the main
#   executing body of the application. This class inherits from the ThreatConnect-specified ExternalApp.
# Parameters :
#   (None)
# Instantiated Object : 
#   An App object containing the main execution code in the run() function as well as an initialized tcex object to interact with the 
#   ThreatConnect API.
class App(JobApp):
    """External App"""

    # Function init()
    # Purpose : 
    #   Initializes the App object and, if running locally, reads the VT API key from the local file in the same folder as app.py named "vtapi.config"
    # Parameters : 
    #   _tcex : the initialized tcex object to interact with the TC API
    # Returns : 
    #   (None)

    def __init__(self, _tcex: object):
        """Initialize class properties."""
        
        
        super().__init__(_tcex)
        
        self.begin = time.time()
        
    # Function run()
    # Purpose : 
    #   This is the main execution loop of the application. It first attempts to read from results_tc to get the last notification ID. It then grabs all notifications 
    #   using the global get_notificaitons function. It then cycles over each notification and instantiates a new Notification object, which by default adds all 
    #   relevant data to the platform.
    
    def run(self) -> None:
        """Run main App logic."""
        self.tcex.log.info('[-] Notification sync starting')
        

        #Check for last_id, push if it doesn't exist
        if self.args.last_cursor != 0:
            last_id = self.args.last_cursor

        if self.args.last_cursor == False or self.args.last_cursor == '0':
            #First Run
            self.tcex.log.error('[x] last_id not found!')
            self.tcex.log.info('[-] Cursor: '+str(last_id))
            notifications = get_notifications(vt_api_key=self.args.vt_api_key)
        
        else:
            last_id = self.args.last_cursor
            self.tcex.log.info(f'[-] last_id found, value: {str(last_id)}')
            notifications = get_notifications(vt_api_key=self.args.vt_api_key)
        

        

        #This section returns the data structures returned by the get_notifications() function, which 
        #returns an iterable data structure full of VT Notification data structures with a default limit 
        #of 40 Notifications per iteration. 
        

        data = next(notifications)
        
        #This section iterates over the entirety of the notifications and trims it to the last_id value 
        count = 0
        last_found = False
        desc_count = 0
        priority_count = 0
        confidence_count = 0
        vt_ruleids = {}
        vt_ruleids_count = 0
        vt_rules_count = 0
        stop = False


        try:
            last_id = data['meta']['cursor']
            data = data['data']
            first_id = data[0]['context_attributes']['notification_id']
            
            
        except Exception as e:
            self.tcex.log.info('[-] No new notifications!')
            self.tcex.log.info('[x] Exception: '+str(e))
            self.tcex.log.info('[x] Data: '+str(data))
            data = {}
            self.tcex.results_tc('last_cursor', last_id)
        notification_objs = []
        while data:
            if last_id:
                ids = [i['context_attributes']['notification_id'] for i in data]
            for point in data:
                count = count+1
                date = int(datetime.datetime.now(datetime.timezone.utc).timestamp())
                try:
                    if date - point['context_attributes']['notification_date'] > daystoseconds(1):
                        
                        stop = True
                except Exception as e:
                    self.tcex.log.info('[x] Exception in date finding: '+str(e))
                    self.tcex.log.info('[x] Point: '+str(point))
                    self.tcex.log.info('[x] Context attributes: '+str(point['context_attributes']))
                tcobj = {}
                
                if TEST and point['context_attributes']['ruleset_name'] == 'VT Notification Test Ruleset':
                    notification_obj = Notification(point, self.args.vt_api_key, self.tcex, self.args)
                    notification_objs.append(notification_obj)
                elif not TEST and point['context_attributes']['ruleset_name'] != 'VT Notification Test Ruleset':
                    notification_obj = Notification(point, self.args.vt_api_key, self.tcex, self.args)
                    notification_objs.append(notification_obj)
                # if stop:
                #     last_noti_id = notification_obj.notification_id
                #     break
                try:
                    data = next(notifications)
                    data = data['data']
                except:
                    break
            # End of point loop
            if stop:
                break
                

        self.tcex.results_tc('last_cursor', last_id)
        self.tcex.log.info('[-] last_id set to: '+str(last_id))
        # End of data loop    
        self.tcex.log.info('[-] Processed {} notifications, a total of {} had descriptions present, a total of {} had priority present, and a total of {} had confidence present.'.format(str(count), str(desc_count), str(priority_count), str(confidence_count)))    
        
        self.tcex.log.info('[-] Length of notification_objs: {}'.format(len(str(notification_objs))))
        end = time.time()
        self.tcex.log.info(f'[-] Run time: {str(end-self.begin)}')    

        
       